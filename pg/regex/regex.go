package main

import (
	"fmt"
	"regexp"
)

func main() {

	reDia := regexp.MustCompile(`[À-ž]`)

	someString := `áËž`

	result1 := reDia.FindAllIndex([]byte(someString), -1)
	result2 := reDia.FindAll([]byte(someString), -1)
	result3 := reDia.Match([]byte(someString))

	fmt.Println(len(result1), len(result2), result3)

}
