package UIC9183

import (
	"reflect"
	"testing"
)

func Test_setUtlayTicketFields(t *testing.T) {
	type args struct {
		fieldsString string
	}
	tests := []struct {
		name string
		args args
		want []Field
	}{
		{name: `1 veld`,
			args: args{fieldsString: `0019013200011Dag Daluren`},
			want: []Field{{
				RowNum:     0,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				Length:     11,
				Value:      "Dag Daluren",
			}},
		},

		{name: `2 velden`,
			args: args{fieldsString: `0019013200011Dag Daluren0119013200012Dag Daluren2`},
			want: []Field{{
				RowNum:     0,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				Length:     11,
				Value:      "Dag Daluren",
			}, {
				RowNum:     1,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				// lengte aangepast
				Length: 12,
				//string aangepast
				Value: "Dag Daluren2",
			},
			},
		},
		{name: `Diacrieten`,
			args: args{fieldsString: `0019013200011Dâg Dälúrén0119013200012Dag Daluren2`},
			want: []Field{{
				RowNum:     0,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				Length:     11,
				Value:      "Dâg Dälúrén",
			}, {
				RowNum:     1,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				// lengte aangepast
				Length: 12,
				//string aangepast
				Value: "Dag Daluren2",
			},
			},
		},
		{name: `Diacrieten laatste veld`,
			args: args{fieldsString: `0119013200012Dag Dalurenx0019013200015Dâg Dälúrén`},
			want: []Field{{
				RowNum:     1,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				Length:     12,
				Value:      "Dag Dalurenx",
			}, {
				RowNum:     0,
				ColNum:     19,
				Height:     1,
				Width:      32,
				Formatting: 0,
				Length:     15,
				Value:      "Dâg Dälúrén",
			},
			},
		},
		{name: `Diacrieten en laatste karakters zijn cijfers --> 2022`,
			args: args{fieldsString: `0301015040033Gültigkeit 20.09.2022-21.09.20220613011900006Berlin`},
			want: []Field{{
				RowNum:     3,
				ColNum:     01,
				Height:     1,
				Width:      50,
				Formatting: 4,
				Length:     33,
				Value:      "Gültigkeit 20.09.2022-21.09.2022", //03 01 01 50 4 0033
			}, {
				RowNum:     6,
				ColNum:     13,
				Height:     1,
				Width:      19,
				Formatting: 0,
				Length:     6,
				Value:      "Berlin", //06 13 01 19 0 0006 Berlin
			},
			}}}

	//aantal velden voor test op 99
	for _, tt := range tests {
		t.Run(tt.name, func(t *testing.T) {
			if got := setUtlayTicketFields(tt.args.fieldsString, 99); !reflect.DeepEqual(got, tt.want) {
				t.Errorf("setUtlayTicketFields() = %v, want %v", got, tt.want)
			}
		})
	}
}
