package UIC9183

import (
	"bytes"
	"strings"
)

type Uhead struct {
	RecordID        string `json:"record_id"`
	RecordVersion   int    `json:"record_version"`
	RecordLength    int    `json:"record_length"`
	Rics            int    `json:"rics"`
	TicketKey       string `json:"ticket_key"`
	EditionTime     string `json:"edition_time"`
	Flag            int    `json:"flag"`
	EditionLanguage string `json:"edition_language"`
	SecondLanguage  string `json:"second_language"`
}

func (t *Format9183) setUHead(result []byte) {
	t.UHead.RecordID = string(result)[:6]
	t.UHead.RecordVersion = bytetoInt((result)[6:8])
	t.UHead.RecordLength = bytetoInt((result)[8:12])
	t.UHead.Rics = bytetoInt((result)[12:16])
	// t.UHead.TicketKey = strings.TrimSpace(string(result)[16:36])
	t.UHead.TicketKey = fixTicketID(result)
	t.UHead.EditionTime = string(result)[36:48]
	t.UHead.Flag = bytetoInt((result)[48:49])
	t.UHead.EditionLanguage = string(result)[49:51]
	t.UHead.SecondLanguage = string(result)[51:53]

}

func fixTicketID(result []byte) string {
	TicketIDbyte := bytes.Trim(result[16:36], "\x00")
	TicketID := string(TicketIDbyte)

	TicketID = strings.TrimSpace(TicketID)

	return TicketID
}
