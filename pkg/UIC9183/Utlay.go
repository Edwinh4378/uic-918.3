package UIC9183

import (
	"fmt"
	"log"
	"regexp"
	"strings"
)

type Utlay struct {
	RecordID       string  `json:"record_id"`
	RecordVersion  int     `json:"record_version"`
	RecordLength   int     `json:"record_length"`
	LayoutStandard string  `json:"layout_standard"`
	NumberOfFields int     `json:"number_of_fields"`
	Fields         []Field `json:"fields"`
}

type Field struct {
	RowNum     int    `json:"row_num"`
	ColNum     int    `json:"col_num"`
	Height     int    `json:"height"`
	Width      int    `json:"width"`
	Formatting int    `json:"formatting"`
	Length     int    `json:"length"`
	Value      string `json:"value"`
}

func (t *Format9183) setUTlay(result []byte) {
	t.Utlay.RecordID = string(result)[:6]
	t.Utlay.RecordVersion = bytetoInt((result)[6:8])
	t.Utlay.RecordLength = bytetoInt((result)[8:12])
	t.Utlay.LayoutStandard = string((result)[12:16])
	t.Utlay.NumberOfFields = bytetoInt((result)[16:20])

	fieldsString := string((result)[20:])
	fmt.Println(fieldsString)

	t.Utlay.Fields = setUtlayTicketFields(fieldsString, t.Utlay.NumberOfFields)
}

func setUtlayTicketFields(fieldsString string, NumberOfFields int) []Field {
	var fields []Field
	var fieldcount int = 1
	// use length of string found
	for len(fieldsString) > 13 && fieldcount <= NumberOfFields {
		// check if elementInfo only contains digits.
		elementInfo := fieldsString[:13]

		re := regexp.MustCompile(`\d{13}`)
		if !re.Match([]byte(elementInfo)) {
			log.Println("Element information contains non digits, previous field is not parsed correctly")
			break
		}
		length := stringtoInt(fieldsString[9:13])

		fieldEnd := 13 + length
		// check if newvalue contains diakritics, because possibility of stringlength problems
		newValue := fieldsString[13:fieldEnd]

		regDiactrics := regexp.MustCompile(`[À-ž]`)
		amountFoundDiactrics := len(regDiactrics.FindAll([]byte(newValue), -1))
		if amountFoundDiactrics >= 1 { //check for diactrics
			//looking up elementdata NEXT field to know where current field actually ends.

			//had to take one extra character of next field to fix when field ends with with digid
			re := regexp.MustCompile(`\d{13}\D|$`)

			firstMostRight := re.FindAll([]byte(fieldsString[13:]), -1)
			//postition in fieldsString; this will be the endpoint

			if len(firstMostRight) != 1 {
				strippedFieldstring := fieldsString[13:]
				strpos := strings.Index(strippedFieldstring, string(firstMostRight[0]))
				//overwrite old newValue
				newValue = strippedFieldstring[:strpos]
			}

			if len(newValue) != length {
				log.Println("Value contained diacritics, and given field length was not the same as actual length of field, fixing the problem")
				fieldEnd = 13 + len(newValue)
			}
			fieldcount++
		}

		new := Field{
			RowNum:     stringtoInt(fieldsString[:2]),
			ColNum:     stringtoInt(fieldsString[2:4]),
			Height:     stringtoInt(fieldsString[4:6]),
			Width:      stringtoInt(fieldsString[6:8]),
			Formatting: stringtoInt(fieldsString[8:9]),
			Length:     length,
			Value:      newValue,
		}

		fieldsString = fieldsString[fieldEnd:]

		fields = append(fields, new)
	}
	return fields

}

func (u Utlay) getValueByRowColNum(rowNum int, colNum int) string {

	for _, v := range u.Fields {
		if v.ColNum == colNum && v.RowNum == rowNum {
			return v.Value
		}
	}
	return `No Value`
}

type Mapping struct {
	RowNum    int
	ColumnNum int
}

// func init() {
// 	setRicsMapping()
// }

var ricsmapping map[int]map[string]Mapping

func setRicsMapping() {
	// map ricss --> map fieldnames --> mapping struct
	// ricsmapping := make(map[int]map[string]Mapping)

	ricsmapping[123]["Validity"] = Mapping{
		RowNum:    123,
		ColumnNum: 456,
	}
	ricsmapping[123]["Tickettitle"] = Mapping{
		RowNum:    333,
		ColumnNum: 444,
	}

}

// get row and column
func getDataPosition(rics int, fieldname string) (int, int) {
	val := ricsmapping[rics][fieldname]
	return val.RowNum, val.ColumnNum

}
