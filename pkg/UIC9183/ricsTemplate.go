package UIC9183

type ricsTemplates struct {
	ricsTemplates []ricsTemplate
}

type ricsTemplate struct {
	rics   string
	fields []fieldMapping
}

type fieldMapping struct {
	name   string
	row    int
	column int
}

func AddRicsMapping() {}
