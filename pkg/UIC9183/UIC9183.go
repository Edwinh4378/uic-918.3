package UIC9183

import (
	"bytes"
	"compress/zlib"
	"encoding/base64"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"log"
	"strings"
)

type Format9183 struct {
	UniqueMessageTypeID       string `json:"unique_message_type_id,omitempty"`
	MessageTypeVersion        int    `json:"message_type_version,omitempty"`
	RicsCodeOfRU              int    `json:"rics_code_of_ru,omitempty"`
	TicketKeyID               string `json:"ticket_key_id,omitempty"`
	Signature                 string `json:"signature,omitempty"`
	LengthOfCompressedMessage int    `json:"length_of_compressed_message,omitempty"`
	CompressedMessage         string `json:"compressed_message,omitempty"`
	DecompressedMessage       string `json:"decompressed_message,omitempty"`
	UHead                     Uhead  `json:"u_head,omitempty"`
	Utlay                     Utlay  `json:"utlay,omitempty"`
}

var data []byte

func (t *Format9183) SetDataFieldsFromRAW(data []byte) Format9183 {

	t.UniqueMessageTypeID = string(data[:3])
	t.MessageTypeVersion = bytetoInt(data[3:5])
	t.RicsCodeOfRU = bytetoInt((data[5:9]))
	t.TicketKeyID = strings.TrimSpace(string(data[9:14]))

	//set seal / signature --> length of signature is given by signature
	seallength := data[15] + 2
	t.Signature = base64.StdEncoding.EncodeToString(data[14 : 14+seallength])

	t.LengthOfCompressedMessage = bytetoInt(data[64:68])
	fmt.Println(t.LengthOfCompressedMessage)

	actualLengthCompressedMessage := len(data[68:])
	if t.LengthOfCompressedMessage-actualLengthCompressedMessage == 1 {
		log.Printf(`length %d is 1 byte difference, found length %d will be taken as actual length`, t.LengthOfCompressedMessage, actualLengthCompressedMessage)

		t.LengthOfCompressedMessage = actualLengthCompressedMessage

	}

	CompressedMessage := data[68 : 68+t.LengthOfCompressedMessage]

	t.CompressedMessage = base64.StdEncoding.EncodeToString(CompressedMessage)

	result := t.decompressData(CompressedMessage)

	log.Println(base64.StdEncoding.EncodeToString(result))

	t.setUHead(result)
	// t.setUTlay(result[t.UHead.RecordLength:]) //?/
	t.setUTlay(result[strings.Index(string(result), "U_TLAY"):])

	return *t

}

func (t *Format9183) decompressData(CompressedMessage []byte) []byte {
	r, err := zlib.NewReader(bytes.NewReader(CompressedMessage))
	if err != nil {
		log.Println("a decompression problem occured")
	}

	result, _ := ioutil.ReadAll(r)

	t.DecompressedMessage = string(result)
	return result
}

func (t *Format9183) CreateJson() []byte {
	ticketData, err := json.Marshal(t)
	if err != nil {
		panic(err)
	}

	return ticketData

}
