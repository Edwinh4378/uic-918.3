package UIC9183

import "testing"

func TestUtlay_getValueByRowColNum(t *testing.T) {
	type fields struct {
		RecordID       string
		RecordVersion  int
		RecordLength   int
		LayoutStandard string
		NumberOfFields int
		Fields         []Field
	}
	type args struct {
		rowNum int
		colNum int
	}
	tests := []struct {
		name   string
		fields fields
		args   args
		want   string
	}{
		// TODO: Add test cases.
		{
			name: "get value 123456 on row 123 and col 456",
			fields: fields{
				RecordID:       "999",
				RecordVersion:  999,
				RecordLength:   999,
				LayoutStandard: "999",
				NumberOfFields: 999,
				Fields: []Field{{
					RowNum:     123,
					ColNum:     456,
					Height:     0,
					Width:      0,
					Formatting: 0,
					Length:     0,
					Value:      "123456",
				}},
			},
			args: args{
				rowNum: 123,
				colNum: 456,
			},
			want: "123456",
		},
	}

	for _, tt := range tests {

		t.Run(tt.name, func(t *testing.T) {
			u := Utlay{
				RecordID:       tt.fields.RecordID,
				RecordVersion:  tt.fields.RecordVersion,
				RecordLength:   tt.fields.RecordLength,
				LayoutStandard: tt.fields.LayoutStandard,
				NumberOfFields: tt.fields.NumberOfFields,
				Fields:         tt.fields.Fields,
			}
			if got := u.getValueByRowColNum(tt.args.rowNum, tt.args.colNum); got != tt.want {
				t.Errorf("Utlay.getValueByRowColNum() = %v, want %v", got, tt.want)
			}
		})
	}
}
