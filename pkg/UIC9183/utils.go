package UIC9183

import (
	"encoding/base64"
	"log"
	"strconv"
)

func base64decodestring(s string) []byte {
	// ds, err := base64.StdEncoding.DecodeString(s)
	ds, err := base64.StdEncoding.WithPadding(base64.NoPadding).DecodeString(s)

	if err != nil {
		log.Println(`Base64 decode fails`, err)
	}

	return ds
}
func bytetoInt(data []byte) int {
	dataint, err := strconv.Atoi(string(data))

	if err != nil {
		log.Println("error:", err)
		log.Println(data, " : value set to 0")
		dataint = 0

	}
	return dataint
}

func stringtoInt(datastring string) int {
	dataint, err := strconv.Atoi(datastring)

	if err != nil {
		log.Println("error:", err)

	}
	return dataint

}
