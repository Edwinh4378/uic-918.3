FROM golang:1.17.5-alpine AS builder

WORKDIR /app

COPY go.mod ./
COPY go.sum ./
RUN  go mod download

COPY cmd/*.go ./
COPY ExampleResponses ExampleResponses



RUN go build -o /lsf-stub


FROM alpine:latest 
# RUN apk --no-cache add ca-certificates
WORKDIR /root/

COPY --from=builder /lsf-stub ./
COPY --from=builder /app/ExampleResponses ExampleResponses





# COPY --from=builder /lsf-stub/ dest

EXPOSE 80

CMD ["./lsf-stub"]
